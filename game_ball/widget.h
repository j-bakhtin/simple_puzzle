#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QLabel>

#include "ball.h"
#include "myscene.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void start(int record, int time);

private slots:
    /**
     * @brief Окончание игры
     */
    void endGame();

    /**
     * @brief Увеличить счетчик лопнутых шаров
     */
    void addCounter();

    /**
     * @brief обновить таймер оставшегося на игру времени
     */
    void displayTimer();

signals:
    void back(int);
    void exit();

private:
    MyScene *scene;
    QTimer *round_timer;

    int time;
    int record = 0;
    int counter = 0;
    int timer = 0;

    QGraphicsView *graphicsView;

    QLabel *recordLabel;
    QLabel *counterLabel;
    QLabel *timerLabel;
};

#endif // WIDGET_H
