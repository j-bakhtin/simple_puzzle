#ifndef MOVEITEM_H
#define MOVEITEM_H

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <mutex>

class Ball : public QGraphicsItem
{
public:
    Ball(QPointF pos, QGraphicsItem *parent = 0);

    /**
     * @brief Получить плотность шарика
     */
    int getDenesity();

private:
    qreal radius;
    QColor color;
    int density;

    QPainterPath shape() const;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // MOVEITEM_H
