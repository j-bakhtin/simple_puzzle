#ifndef MYSCENE_H
#define MYSCENE_H

#include <QGraphicsScene>
#include <vector>
#include <thread>
#include <atomic>

#include "ball.h"

const double OFFSET = 0.2;
const int SPEED = 16;

class MyScene : public QGraphicsScene{
    Q_OBJECT
public:
    MyScene(QObject *parent = Q_NULLPTR);
    ~MyScene();

    /**
     * @brief Добавить новый шарик
     * @param[in] pos координаты нового шарика
     */
    void addBall(QPointF pos);

    /**
     * @brief Удалить шарик
     * @param[in] ball указатель на шарик, которые надо удалить
     */
    void removeBall(Ball *ball);

    /**
     * @brief Удалить все шарики
     */
    void removeAllBalls();

    /**
     * @brief Начать
     */
    void start();

public slots:
    /**
     * @brief Сгенерировать шарик
     */
    void ball_generate();

    /**
     * @brief Обновить позицию шарика на сцене
     */
    void update_balls();

protected:
    /**
     * @brief Нажатие по мыши
     */
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);

signals:
    /**
     * @brief Сигнал о том что шарик лопнул (для увеличения счетчика лопнутых шаров)
     */
    void ballDestroyed();

private:
    std::vector<Ball *> balls;
};

#endif // MYSCENE_H
