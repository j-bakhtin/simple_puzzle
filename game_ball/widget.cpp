#include <QDateTime>

#include <qboxlayout.h>
#include <qgraphicsview.h>
#include <qlayoutitem.h>
#include <qpushbutton.h>

#include "widget.h"
#include "utils.h"

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    // НАСТРОЙКА СЦЕНЫ

    // Настройка header'a
    QLabel *recordTitleLabel = new QLabel(" Рекорд: ");
    recordLabel = new QLabel("-");
    QLabel *counterTitleLabel = new QLabel(" Счет: ");
    counterLabel = new QLabel("-");
    QLabel *timerTitleLabel = new QLabel(" Время: ");
    timerLabel = new QLabel("-");
    QSpacerItem *space = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    QHBoxLayout * header = new QHBoxLayout();
    header->addWidget(recordTitleLabel);
    header->addWidget(recordLabel);
    header->addItem(space);
    header->addWidget(counterTitleLabel);
    header->addWidget(counterLabel);
    header->addWidget(timerTitleLabel);
    header->addWidget(timerLabel);

    //Настройка сцены
    graphicsView = new QGraphicsView();
    graphicsView->setCacheMode(QGraphicsView::CacheBackground);
    graphicsView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    graphicsView->setCacheMode(QGraphicsView::CacheBackground);
    graphicsView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

    // Добвляем все в виджет
    QVBoxLayout *wrapper = new QVBoxLayout();
    wrapper->addItem(header);
    wrapper->addWidget(graphicsView);
    this->setLayout(wrapper);
}

Widget::~Widget(){}

void Widget::start(int _record, int _time){
    // Инициализируем таймер и счетчик лопнутых шаров
    timer = _time;
    counter = 0;

    // Задаем первоначальное значение показателей в header'e
    timerLabel->setNum(timer);
    counterLabel->setNum(counter);
    recordLabel->setNum(_record);

    // Настраиваем сцену
    scene = new MyScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(0, 0, this->width()-160, this->height()-160);
    graphicsView->setScene(scene);

    // Время отведенное на игру
    round_timer = new QTimer();
    connect(round_timer, SIGNAL(timeout()), this, SLOT(displayTimer()));
    round_timer->start(1000);

    // Изсенение счетчика лопнутых шаров
    connect(scene, SIGNAL(ballDestroyed()), this, SLOT(addCounter()));

    scene->start();
}

void Widget::endGame(){
    delete scene;
    delete round_timer;

    emit back(counter);
}

void Widget::addCounter(){
    this->counter++;
    counterLabel->setNum(counter);
}

void Widget::displayTimer(){
    timer--;
    timerLabel->setNum(timer);

    // Завершаем игру по истечении времени
    if(timer <= 0){
        round_timer->stop();
        endGame();
    }
}
