#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <chrono>
#include <qdatetime.h>
#include <qtimer.h>

#include "myscene.h"
#include "utils.h"

MyScene::MyScene(QObject *parent) :
    QGraphicsScene(parent) {}

MyScene::~MyScene()
{
    removeAllBalls();
}

void MyScene::start()
{
    // Генерация шара
    QTimer *ball_generate_timer = new QTimer();
    QObject::connect(ball_generate_timer, SIGNAL(timeout()), this, SLOT(ball_generate()));
    ball_generate_timer->start(500);

    // Обновления позиции шара
    QTimer *tick_timer = new QTimer();
    QObject::connect(tick_timer, SIGNAL(timeout()), this, SLOT(update_balls()));
    tick_timer->start(30);
}

void MyScene::ball_generate()
{
    QPointF pos(randomBetween(30, this->width()-30), this->height()+100);
    this->addBall(pos);
}

void MyScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsItem *item = itemAt(mouseEvent->scenePos(), QTransform());
    Ball *ball = qgraphicsitem_cast<Ball *>(item);

    if (mouseEvent->button() == Qt::LeftButton) {
        if(ball) {
            removeBall(ball);
        }
    }
}

void MyScene::addBall(QPointF pos)
{
    balls.push_back(new Ball(pos));
    addItem(balls.back());
}

void MyScene::removeBall(Ball *ball)
{
    // Находим шар в общем векторе шаров
    // Перемещаем найденый шар в конец вектора
    // Удаляем шар из конца вектора
    // TODO: возможно есть решение лучше
    balls.erase(std::remove(balls.begin(), balls.end(), ball), balls.end());

    removeItem(ball);
    delete ball;

    emit ballDestroyed();
}

void MyScene::removeAllBalls()
{
    clear();
    balls.clear();
    balls.shrink_to_fit();
}

void MyScene::update_balls()
{
    for(size_t i = 0; i < balls.size(); ++i) {
        // Текущее положение шара
        QPointF xy = balls[i]->pos();

        // Смещение по осям
        qreal ax = 0;
        qreal ay = 0;

        // Вычилсяем смещение по оси Y
        ay-= (OFFSET * SPEED * balls[i]->getDenesity());

        // Инициализируем новую точку на сцене с учетом смещения
        QPointF nxy;
        nxy.setX(xy.rx() + ax);
        nxy.setY(xy.ry() + ay);

        balls[i]->setPos({nxy});
    }
}

