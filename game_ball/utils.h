#ifndef UTILS_H
#define UTILS_H

#include <QPointF>
#include <math.h>

inline int randomBetween(int low, int high)
{
    return (qrand() % ((high + 1) - low) + low);
}

#endif // UTILS_H
