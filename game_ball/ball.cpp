#include <QCursor>
#include <QPainter>

#include "ball.h"
#include "utils.h"

Ball::Ball(QPointF pos, QGraphicsItem *parent) :
    QGraphicsItem(parent),
    radius(30)
{
    const int r = randomBetween(0, 255);
    const int g = randomBetween(0, 255);
    const int b = randomBetween(0, 255);
    color = QColor(r, g, b);
    density = randomBetween(1, 3);
    this->setPos(pos);
}

int Ball::getDenesity()
{
    return this->density;
}

QPainterPath Ball::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

QRectF Ball::boundingRect() const
{
    return QRectF(-radius, -radius, 2*radius, 2*radius);
}

void Ball::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::black);
    painter->setBrush(color);
    painter->drawEllipse(-radius, -radius, 2*radius, 2*radius);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void Ball::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    setPos(mapToScene(event->pos()));
}


