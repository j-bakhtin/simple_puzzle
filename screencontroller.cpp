#include "screencontroller.h"
#include <QKeyEvent>

ScreenController::ScreenController(QWidget *parent)
    : ScreensStack(parent) {

  db = new DataBase();
  db->connectToDataBase();

  m_menu = new MainMenuWidget(this);
  connect(m_menu, SIGNAL(imageSelected(QImage)),
          this, SLOT(on_image_selected(QImage)));
  connect(m_menu, SIGNAL(helpSelected()),
          this, SLOT(on_help_selected()));
  connect(m_menu, SIGNAL(back()),
          this, SLOT(close()));
  push(m_menu);

  m_game = new PuzzleGame(this);
  connect(m_game, SIGNAL(back()),  this, SLOT(pop()));
  connect(m_game, SIGNAL(finished(bool)), this, SLOT(puzzle_finished()));
  m_game->hide();

  m_help = new HelpViewer(this);
  connect(m_help, SIGNAL(back()), SLOT(pop()));
  m_help->hide();

  // Инициализируем виджет мини-игры
  m_game_ball = new Widget(this);
  connect(m_game_ball, SIGNAL(back(int)), SLOT(balls_finished(int)));
  m_game_ball->hide();
}


// Обработка события окончания игры в "Пазлы"
void ScreenController::puzzle_finished(){
    pop();
    push(m_game_ball);

    // Инициализируем время игры и ранее кстановленный рекорд
    int record = db->selectBallsRecord();
    int time = 15;

    // Настраиваем виджит мини игры
    m_game_ball->start(record, time);
}

// Обработка события окончания мини-игры в "Воздушные шарики"
void ScreenController::balls_finished(int result){
    pop();

    // Обновляем статисику
    db->updateBallsStatistic(result);
}

void ScreenController::on_image_selected(QImage image) {
  QPixmap pixmap = QPixmap::fromImage(image);

  bool screenOrientation = height() > width();
  bool pixmapOrientation = pixmap.height() > pixmap.width();

  if (screenOrientation != pixmapOrientation) {
    QTransform transform;
    QTransform trans = transform.rotate(90);
    pixmap = pixmap.transformed(trans);
  }

  Mode mode = m_menu->mode();
  m_game->load(pixmap, mode.horizontally, mode.vertically, mode.rotated);
  push(m_game);
}

void ScreenController::on_back_selected() {
  pop();
}

void ScreenController::on_help_selected() {
  push(m_help);
}

void ScreenController::keyPressEvent(QKeyEvent *event) {
  if (event->key() == Qt::Key_Back) {
    if (lenght() > 1)
      pop();
    else
      this->close();
  }
  event->accept();
}
