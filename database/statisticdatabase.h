#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QDebug>

const QString DATABASE_HOSTNAME = "ExampleDataBase";
const QString DATABASE_NAME = "puzzle.db";

const QString TABLE = "statistics";
const QString TABLE_KEY = "key";
const QString TABLE_VALUE = "value";

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = 0);
    ~DataBase();

    /* Методы для непосредственной работы с классом
     * Подключение к базе данных и вставка записей в таблицу
     */

    /**
     * @brief Методы для подключения к базе данных
     */
    void connectToDataBase();

    /**
     * @brief Метод для обновления записи в базе данных
     */
    void updateBallsStatistic(int value);

    /**
     * @brief Метод для обновления записи в базе данных по ключу
     */
    bool updateRowWhereKey(const QVariantList &data);

    /**
     * @brief Выбрать статистику по игре в шары
     */
    int selectBallsRecord();

private:
    QSqlDatabase db;

private:
    /**
     * @brief Открыть базу данных
     */
    bool openDataBase(); 
};

#endif // DATABASE_H
