#include "database/statisticdatabase.h"

DataBase::DataBase(QObject *parent) : QObject(parent)
{

}

DataBase::~DataBase(){}

void DataBase::connectToDataBase()
{
    // Перед подключением к базе данных производим проверку на её существование
    if(QFile("./" + DATABASE_NAME).exists()){
        this->openDataBase();
    } else {
        qDebug() << "База данных не найдена";
    }
}

bool DataBase::openDataBase()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName(DATABASE_HOSTNAME);
    db.setDatabaseName("./" + DATABASE_NAME);
    if(db.open()){
        qDebug() << "База данных открыта";
        return true;
    } else {
        qDebug() << "Не удалось открыть базу данных";
        return false;
    }
}

void DataBase::updateBallsStatistic(int value){
    qDebug() << value;
    QVariantList data;

    if (value > selectBallsRecord()){
        data.append("balls_record");
        data.append(value);
        updateRowWhereKey(data);
    }
}

bool DataBase::updateRowWhereKey(const QVariantList &data)
{
    QSqlQuery query;

    query.prepare("UPDATE " + TABLE + " SET value=:Value WHERE key=:Key");
    query.bindValue(":Key", data[0].toString());
    query.bindValue(":Value", data[1].toInt());

    if(!query.exec()){
        qDebug() << "error update " << TABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

int DataBase::selectBallsRecord()
{
    QSqlQuery query;

    query.prepare("SELECT value FROM " + TABLE + " WHERE key=:Key");
    query.bindValue(":Key", "balls_record");

    if(!query.exec()){
        qDebug() << "error select from " << TABLE;
        qDebug() << query.lastError().text();
        return 0;
    } else {
        query.first();
        qDebug() << query.value(0).toInt();
        return query.value(0).toInt();
    }
}
