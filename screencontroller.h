#ifndef SCREENCONTROLLER_H
#define SCREENCONTROLLER_H

#include "menu/mainmenuwidget.h"
#include "game/puzzlegame.h"
#include "tutorialviewer/helpviewer.h"
#include "game_ball/widget.h"
#include "screensstack.h"
#include "database/statisticdatabase.h"

class ScreenController : public ScreensStack {
  Q_OBJECT
public:
  explicit ScreenController(QWidget *parent = nullptr);
public slots:
  void on_back_selected();
  void on_image_selected(QImage image);
  void on_help_selected();
  void puzzle_finished();
  void balls_finished(int);
protected:
  void keyPressEvent(QKeyEvent *event);
private:
  MainMenuWidget *m_menu;
  PuzzleGame *m_game;
  HelpViewer *m_help;
  Widget *m_game_ball;
  DataBase *db;
};

#endif // SCREENCONTROLLER_H
